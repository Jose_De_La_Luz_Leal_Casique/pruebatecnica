# PRUEBA TÉCNICA

## Objetivo
    Dar solución a problemática establecida

### Instalar paquetes necesarios para el proyecto, se requieren lo siguiente:
* Instalar dependencias: *pip install -r requirements*
* npm install

### Para ejecutar el proyecto, se necesita:
* python manage.py migrate
* python manage.py runserver
* acceder a URL: *localhost:8000*


## Se decidió usar dichas tecnologías por las siguientes razones:
* React
    * La carga de informacion en componentes facilita la petición y visualización de dicha información
    
* Django-Rest-Framework
    * Crear un API, para ser consumidas mediante axios
    
* SQLITE3
    * Ya que era una poryecto de práctica, sin muchas información
    * La Entidad raiz almacenrá los nombres de empresas
    


