from django.db import models


class Business(models.Model):
    name = models.CharField(
        verbose_name='Nombre de Empresa', null=False, blank=False, max_length=100, unique=True)

    def __str__(self):
        return f'{self.pk}: {self.name}'

    class Meta:
        ordering = ['pk']
        verbose_name = 'Empresa'
        verbose_name_plural = 'Listado de Empresas'


class Departament(models.Model):
    business = models.ForeignKey(Business, on_delete=models.CASCADE, related_name='departament')
    name = models.CharField(verbose_name='Nombre de Departamento', null=False, blank=False, max_length=100)

    def __str__(self):
        return f'({self.pk}) -> {self.name}: {self.business}'

    class Meta:
        ordering = ['pk']
        verbose_name = 'Departamento'
        verbose_name_plural = 'Listado de Departamentos'
