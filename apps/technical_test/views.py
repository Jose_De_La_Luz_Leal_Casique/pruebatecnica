from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response

from apps.accounts.models import User, Profile
from . import serializers
from .models import Business, Departament


class GeneralAddView(generics.CreateAPIView):
    permission_classes = [AllowAny, ]

    @staticmethod
    def response_400(errors):
        return Response(errors, status=status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def response_201(data):
        return Response(data, status=status.HTTP_201_CREATED)

    @staticmethod
    def validate_and_create_object(serializer):
        if serializer.is_valid():
            return serializer.create(serializer.data)

        return None


class AddBusinessView(GeneralAddView):
    serializer_class = serializers.AddBusinessSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if self.validate_and_create_object(serializer) is not None:
            return self.response_201(serializer.data)

        return self.response_400(serializer.errors)


class AddDepartamentView(GeneralAddView):
    serializer_class = serializers.AddDepartamentSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if self.validate_and_create_object(serializer) is not None:
            return self.response_201(serializer.data)

        return self.response_400(serializer.errors)


class AddEmployeeView(GeneralAddView):
    serializer_class = serializers.AddEmployeeSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if self.validate_and_create_object(serializer) is not None:
            return self.response_201(serializer.data)

        return self.response_400(serializer.errors)


class UpdateEmployeeView(generics.UpdateAPIView):
    serializer_class = serializers.UpdateEmployeeSerializer

    def get_object(self):
        return Profile.objects.get(pk=self.kwargs['pk'])

    def update(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            instance = self.get_object()
            serializer.update(instance, serializer.data)
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DeleteEmployee(generics.DestroyAPIView):
    serializer_class = None

    def get_object(self):
        return User.objects.get(pk=self.kwargs['pk'])

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()

    def delete(self, request, *args, **kwargs):
        user = self.get_object()
        self.perform_destroy(user)
        return Response(status=status.HTTP_204_NO_CONTENT)


class ListBusinessView(generics.ListAPIView):
    serializer_class = serializers.BusinessSerializer

    def get_queryset(self):
        return Business.objects.all()


class ListDepartamentView(generics.ListAPIView):
    serializer_class = serializers.DepartamentSerializer

    def get_queryset(self):
        return Departament.objects.filter(business_id=self.kwargs['pk'])


class ListEmployeeView(generics.ListAPIView):
    serializer_class = serializers.ListEmployeeSerializef

    def get_queryset(self):
        return Profile.objects.filter(
            name__icontains=self.kwargs['employee'],
            user__business=self.kwargs['business'],
            user__business__departament=self.kwargs['departament']
        )


class GetEmployeeView(generics.RetrieveAPIView):
    serializer_class = serializers.ListEmployeeSerializef

    def get_object(self):
        try:
            return Profile.objects.get(pk=self.kwargs['pk'])

        except Profile.DoesNotExist:
            return None

    def retrieve(self, request, *args, **kwargs):
        user = self.get_object()

        if user is not None:
            return Response({'user': serializers.ListEmployeeSerializef(
                user, context=self.get_serializer_context()).data}, status=status.HTTP_200_OK)

        return Response(status=status.HTTP_204_NO_CONTENT)
