from django.urls import path

from . import views


urlpatterns = [
    path('business/add/', views.AddBusinessView().as_view(), name='add_business'),
    path('business/list/', views.ListBusinessView.as_view(), name='list_business'),

    path('departament/add/', views.AddDepartamentView().as_view(), name='add_departament'),
    path('departament/list/<int:pk>/', views.ListDepartamentView.as_view(), name='list_departament'),

    path('employee/<int:pk>/', views.GetEmployeeView().as_view(), name='edit_employee'),
    path('employee/add/', views.AddEmployeeView().as_view(), name='add_employee'),
    path('employee/edit/<int:pk>/', views.UpdateEmployeeView().as_view(), name='edit_employee'),
    path('employee/delete/<int:pk>/', views.DeleteEmployee().as_view(), name='edit_employee'),
    path('employee/list/<int:business>/<int:departament>/<str:employee>/',
         views.ListEmployeeView().as_view(), name='edit_employee'),
]
