from django.apps import AppConfig


class TechnicalTestConfig(AppConfig):
    name = 'technical_test'
