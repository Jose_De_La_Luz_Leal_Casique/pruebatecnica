from rest_framework import serializers

from apps.accounts.models import User, Profile
from .models import Business, Departament


class BusinessSerializer(serializers.ModelSerializer):

    class Meta:
        model = Business
        fields = '__all__'


class DepartamentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Departament
        fields = '__all__'


class AddBusinessSerializer(serializers.ModelSerializer):

    def validate(self, attrs):
        instance = Business.objects.filter(name__icontains=attrs.get('name'))

        if instance.exists():
            raise serializers.ValidationError({'errors': 'Ya existe esa empresa'})

        return super().validate(attrs)

    def create(self, validated_data):
        return Business.objects.create(**validated_data)

    class Meta:
        model = Business
        fields = ['name', ]


class AddDepartamentSerializer(serializers.ModelSerializer):

    def validate(self, attrs):
        name = attrs.get('name')
        business = attrs.get('business')
        instance = Departament.objects.filter(name__icontains=name, business_id=business)

        if instance.exists():
            raise serializers.ValidationError({'errors': 'Esta empresa ya tiene departamento de '+name})

        return super().validate(attrs)

    @staticmethod
    def get_business(business):
        return Business.objects.get(pk=business)

    def create(self, validated_data):
        departament = Departament.objects.create(
            name=validated_data.get('name'),
            business=self.get_business(validated_data.get('business'))
        )
        return departament

    class Meta:
        model = Departament
        fields = ['name', 'business']


class AddEmployeeSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(required=False)
    business = serializers.IntegerField(required=True)
    departament = serializers.IntegerField(required=True)
    name = serializers.CharField(required=True, max_length=60)
    paternal_surname = serializers.CharField(required=True, max_length=60)
    maternal_surname = serializers.CharField(required=True, max_length=60)
    birthday = serializers.DateTimeField(required=True)
    gender = serializers.IntegerField(required=False)
    phone = serializers.CharField(required=False, max_length=10)
    cell_phone = serializers.CharField(required=False, max_length=10)
    is_administrator = serializers.BooleanField(required=True)

    def validate(self, attrs):
        is_administrator = attrs.get('is_administrator')

        if is_administrator:
            if attrs.get('password') is None:
                raise serializers.ValidationError({'errors': 'Se requiere contraseña'})

        user = User.objects.filter(email=attrs.get('email'))

        if user.exists():
            raise serializers.ValidationError({'errors': 'Este Correo Electrónico ya fue registrado'})

        return super().validate(attrs)

    @staticmethod
    def get_business(business):
        return Business.objects.get(pk=business)

    @staticmethod
    def get_departament(departament):
        return Departament.objects.get(pk=departament)

    def create_user(self, email, password, business, departament):
        user = User.objects.create_user(
            email=email,
            business=self.get_business(business),
            departament=self.get_departament(departament))
        user.set_password(password)
        return user

    def create(self, validated_data):
        user = self.create_user(
            validated_data.get('email'),
            validated_data.get('password'),
            validated_data.get('business'),
            validated_data.get('departament')
        )
        gender = ''

        if validated_data.get('gender') == 1:
            gender = 'Masculino'

        elif validated_data.get('gender') == 2:
            gender = 'Femenino'

        Profile.objects.create(
            user=user,
            name=validated_data.get('name'),
            paternal_surname=validated_data.get('paternal_surname'),
            maternal_surname=validated_data.get('maternal_surname'),
            birthday=validated_data.get('birthday'),
            gender=gender,
            phone=validated_data.get('phone'),
            cell_phone=validated_data.get('cell_phone'),
            date_admission=validated_data.get('date_admission')
        )

        return user


class UpdateEmployeeSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=False)
    paternal_surname = serializers.CharField(required=False)
    maternal_surname = serializers.CharField(required=False)
    phone = serializers.CharField(required=False)
    cell_phone = serializers.CharField(required=False)

    def update(self, instance, validated_data):
        print(validated_data)

        if validated_data.get('name') is not None:
            instance.name = validated_data.get('name')

        if validated_data.get('paternal_surname') is not None:
            instance.paternal_surname = validated_data.get('paternal_surname')

        if validated_data.get('maternal_surname') is not None:
            instance.maternal_surname = validated_data.get('maternal_surname')

        if validated_data.get('phone') is not None:
            instance.phone = validated_data.get('phone')

        if validated_data.get('cell_phone') is not None:
            instance.cell_phone = validated_data.get('cell_phone')

        instance.save()
        return instance

    class Meta:
        model = Profile
        fields = ['name', 'paternal_surname', 'maternal_surname', 'phone', 'cell_phone']


class ListEmployeeSerializef(serializers.ModelSerializer):

    class Meta:
        model = Profile
        fields = '__all__'
