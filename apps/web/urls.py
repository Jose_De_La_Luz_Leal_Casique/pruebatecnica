from django.urls import path

from .views import App


urlpatterns = [
    path('', App.as_view(), name='index_front'),
]
