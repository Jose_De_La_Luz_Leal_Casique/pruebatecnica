import { LOGIN_FAIL, LOGIN_SUCCESS, USER_LOADED, USER_LOADING, AUTH_ERROR } from '../actions/types';

const initialState = {
    isAuthenticated: null,
    isLoading: false,
    user: null,
};

export default function (state = initialState, action) {
    switch (action.type) {

        case LOGIN_SUCCESS:
            localStorage.setItem('token', action.payload.token);
            return {
                ...state,
                isAuthenticated: true,
                user: action.payload
            }

        case LOGIN_FAIL:
        case AUTH_ERROR:
            localStorage.removeItem('token');
            return {
                ...state,
                isAuthenticated: false,
                isLoading: false,
                user: null
            }

        case USER_LOADING:
            return {
                ...state,
                isLoading: true,
            };


        case USER_LOADED:
            return {
                ...state,
                isAuthenticated: true,
                isLoading: false,
                user: action.payload,
            };

        default:
            return state;
    }
}