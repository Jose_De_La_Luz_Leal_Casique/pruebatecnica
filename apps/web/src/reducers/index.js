import { combineReducers } from 'redux';
import errors from './errors';
import messages from './messages';
import auth from './auth'
import business from './test/business'
import departement from "./test/departement";
import employee from "./test/employee";

export default combineReducers({
    errors,
    messages,
    auth,
    business,
    departement,
    employee,
});