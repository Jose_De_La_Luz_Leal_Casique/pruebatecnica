import {REGISTER_DEPARTAMENT, LIST_DEPARTAMENT} from '../../actions/types';

const initialState = {
    list: null,
    load: false
};

export default function (state = initialState, action) {
    switch (action.type) {

        case LIST_DEPARTAMENT:
            return {
                ...state,
                list: action.payload,
                load: true
            };

        case REGISTER_DEPARTAMENT:
        default:
            return state;
    }
}