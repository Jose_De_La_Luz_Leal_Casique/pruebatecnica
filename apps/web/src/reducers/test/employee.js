import { LIST_EMPLOYEE, GET_EMPLOYEE, UPDATE_EMPLOYEE, ADD_EMPLOYEE } from '../../actions/types';

const initialState = {
    list: null,
    edit: null,
    loadEdit: false,
    load: false
};

export default function (state = initialState, action) {
    switch (action.type) {

        case GET_EMPLOYEE:
        case UPDATE_EMPLOYEE:
            return {
                ...state,
                edit: action.payload,
                loadEdit: true
            }

        case LIST_EMPLOYEE:
            return {
                ...state,
                list: action.payload,
                load: true
            };

        case ADD_EMPLOYEE:
            return {
                ...state,
                list:[...state.list, action.payload]
            }

        default:
            return state;
    }
}