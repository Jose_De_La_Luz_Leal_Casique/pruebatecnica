import { REGISTER_BUSINESS, LIST_BUSINESS } from '../../actions/types';

const initialState = {
    list: null,
    load: false
};

export default function (state = initialState, action) {
    switch (action.type) {

        case LIST_BUSINESS:
            return {
                ...state,
                list: action.payload,
                load: true
            };

        case REGISTER_BUSINESS:
        default:
            return state;
    }
}