import React, { Component, Fragment } from 'react';
import { HashRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { Provider as AlertProvider } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';
import { Provider } from 'react-redux';

import store from '../store';
import Alerts from './layout/Alerts';
import PrivateRoute from "./common/PrivateRoute";

import Login from "./accounts/Login";
import Dashboard from "./layout/Dashboard";
import ListFilters from "./layout/ListFilters";
import Edit from "./layout/Edit";
import AddEmployee from "./test/AddEmployee";

import { loadUser } from '../actions/loadUser';

const alertOptions = {
  timeout: 3000,
  position: 'top center',
};

class App extends Component {

    componentDidMount() {
        store.dispatch(loadUser());
    }

    render() {
        return (
            <Provider store={store}>
                <AlertProvider template={AlertTemplate} {...alertOptions}>
                    <Router>
                        <Fragment>
                            <Alerts />
                            <div className="container">
                                <Switch>
                                    <Route exact path="/login" component={Login} />
                                    <PrivateRoute exact path="/" component={Dashboard} />
                                    <PrivateRoute exact path="/list" component={ListFilters} />
                                    <PrivateRoute exact path="/new" component={AddEmployee} />
                                    <PrivateRoute exact path="/edit" component={Edit} />
                                </Switch>
                            </div>
                        </Fragment>
                    </Router>
                </AlertProvider>
            </Provider>
        );
    }
}

export default App;