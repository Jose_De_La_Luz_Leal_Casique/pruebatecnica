import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { login } from '../../actions/login';


export class Login extends React.Component {

    state = {
        email: '',
        password: ''
    }

    static propTypes = {
        login: PropTypes.func.isRequired,
        isAuthenticated: PropTypes.bool,
    };

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmit = (e) => {
        e.preventDefault();
        this.props.login(this.state.email, this.state.password);
    }

    render() {

        if (this.props.isAuthenticated){
            return <Redirect to="/" />;
        }

        return (
            <form className="form-login col-4 text-center" onSubmit={this.onSubmit}>
                <div className="mt-3">
                    <h4 className="m-auto py-3"><strong>Inicio de Sesión</strong></h4>
                </div>
                <hr></hr>

                    <div className="form-group mt-4">
                        <label htmlFor="exampleInputEmail1"><strong>Correo Electrónico</strong></label>
                        <input type="email" className="form-control text-center" name='email'
                               required  onChange={this.onChange}/>
                    </div>

                    <div className="form-group mt-3">
                        <label htmlFor="exampleInputPassword1"><strong>Contraseña</strong></label>
                        <input type="password" className="form-control text-center" name='password'
                               required  onChange={this.onChange}/>
                    </div>

                    <div className="form-group my-3">
                        <button type="submit" className="btn-login"><strong>Entrar</strong></button>
                    </div>

            </form>
        );
    }
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, { login })(Login);
