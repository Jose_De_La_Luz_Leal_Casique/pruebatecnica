import React from 'react';
import { connect } from 'react-redux';
import PropTypes from "prop-types";

import { add_employee } from '../../actions/addEmployee';

import { ConfigDateSave } from '../FormatDate';


export class AddEmployee extends React.Component {

    state = {
        name: "",
        paternal_surname: "",
        maternal_surname: "",
        birthday: "",
        gender: "",
        phone: "",
        cell_phone: "",
        email: "",
        password: "",
        is_administrator: false,
        business: 0,
        departament: 0
    }

    static propTypes = {
        add_employee: PropTypes.func.isRequired,
    };

    componentDidMount() {
        const params = this.props.location.state;
        this.setState({ business: params.business,  departament: params.departament});

    }


    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});

        if (e.target.name === 'is_administrator'){
            this.setState({
                is_administrator: e.target.checked
            })
        }
    }

    onSubmit = (e) =>{
        e.preventDefault();
        this.props.add_employee(
            this.state.name,
            this.state.paternal_surname,
            this.state.maternal_surname,
            ConfigDateSave(this.state.birthday),
            parseInt(this.state.gender),
            this.state.phone,
            this.state.cell_phone,
            this.state.email,
            this.state.password,
            this.state.is_administrator,
            this.state.business,
            this.state.departament
        )

    }


    render() {

        return (
            <div className="row mt-5">
                <form className="col-10 mx-auto mt-3 card py-4" onSubmit={this.onSubmit}>
                    <h5 className="text-center"><strong>Registro de Empleados</strong></h5>

                    <section className="row">
                        <div className="col-4 mx-auto mt-3">
                            <label>Nombre: </label>
                            <input type="text" name="name" className="form-control" required
                                   value={this.state.name} onChange={this.onChange}/>
                        </div>

                        <div className="col-4 mx-auto mt-3">
                            <label>Apellido Paterno:</label>
                            <input type="text" name="paternal_surname" className="form-control" required
                                   value={this.state.paternal_surname} onChange={this.onChange}/>
                        </div>

                        <div className="col-4 mx-auto mt-3">
                            <label>Apellido Materno:</label>
                            <input type="text" name="maternal_surname" className="form-control" required
                                   value={this.state.maternal_surname} onChange={this.onChange}/>
                        </div>

                        <div className="col-4 m-auto">
                            <label>Fecha de Nacimiento:</label>
                            <input type="date" name="birthday" className="form-control" required
                                   value={this.state.birthday} onChange={this.onChange}/>
                        </div>


                        <div className="col-4 m-auto">
                            <label>Número Telefónico:</label>
                            <input type="text" name="phone" className="form-control"
                                   value={this.state.phone} onChange={this.onChange}/>
                        </div>

                        <div className="col-4 mx-auto mt-3">
                            <label>Número de Celular:</label>
                            <input type="text" name="cell_phone" className="form-control"
                                   value={this.state.cell_phone} onChange={this.onChange}/>
                        </div>

                        <div className="col-4 mx-auto mt-3">
                            <label>Género:</label>
                            <select className="form-control" name="gender"
                                value={this.state.gender} onChange={this.onChange}>
                                <option key={0} value={1}>Maculino</option>
                                <option key={1} value={2}>Femenino</option>
                            </select>
                        </div>

                        <div className="col-4 mx-auto mt-3">
                            <label>Correo Electrónico:</label>
                            <input type="email" name="email" className="form-control" required
                                   value={this.state.email} onChange={this.onChange}/>
                        </div>

                        <div className="col-4 mx-auto mt-3">
                            <label>Contraseña:</label>
                            <input type="password" name="password" className="form-control" required
                                   value={this.state.password} onChange={this.onChange}/>
                        </div>

                        <div className="col-4 ml-4 mt-4">
                            <input type="checkbox" name="is_administrator" className="form-check-input"
                                   value={this.state.is_administrator} onChange={this.onChange}/>
                                   <label>Es Administardor:</label>
                        </div>

                    </section>
                    <div className="mt-4">
                        <button className="btn btn-primary col-2 float-right">Guardar</button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
   employee: state.employee
});


export default connect(mapStateToProps, { add_employee })(AddEmployee);