import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";

import { FormatDate } from '../FormatDate';


export class ListEmployee extends Component {

    static propTypes = {
        employee: PropTypes.object.isRequired,
    }

    render() {

        if (this.props.employee.load === true){

            return (
                 <table className="table table-striped mt-5">
                        <thead className="text-center">
                            <tr>
                                <th>Nombre (s)</th>
                                <th >Apellido Paterno</th>
                                <th >Apellido Materno</th>
                                <th>Fecha de Nacimiento</th>
                                <th>Teléfono</th>
                                <th>Aciones</th>
                            </tr>
                        </thead>
                        <tbody className="text-center">
                            {this.props.employee.list.map((obj) => (
                            <tr key={obj.id}>
                                <td>{obj.name}</td>
                                <td>{obj.paternal_surname}</td>
                                <td>{obj.maternal_surname}</td>
                                <td>{FormatDate(obj.birthday)}</td>
                                <td>{obj.phone}</td>
                                <td>
                                    <Link
                                        to={{
                                            pathname: '/edit',
                                            state: {id: obj.id}
                                        }} >Editar
                                    </Link>
                                </td>
                            </tr>
                            ))}
                        </tbody>
                    </table>
            )

        } else {
            return <h3 className="text-center"><strong>Loading...</strong></h3>
        }

    }
}


export default ListEmployee;
