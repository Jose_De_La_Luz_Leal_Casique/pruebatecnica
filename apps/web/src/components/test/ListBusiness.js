import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { loadBusiness } from "../../actions/getListBusiness";


export class ListBusiness extends Component {

    static propTypes = {
        business: PropTypes.object.isRequired,
        loadBusiness: PropTypes.func.isRequired,
        select_business: PropTypes.number
    }

    componentDidMount() {

        if (!this.props.business.load){
            this.props.loadBusiness()
        }
    }

    render() {

        if (this.props.business.load === true){
            return (
                <select className="form-control" name="business"
                        value={this.props.select_business} onChange={this.props.onChange}>
                    {this.props.business.list.map((obj, index) => (
                        <option key={index} value={obj.id}>{obj.name}</option>
                        ))};
                </select>
            );
        } else {
            return <h1 className="text-center">Loading...</h1>
        }

    }
}

const mapStateToProps = (state) => ({
    business: state.business
});

export default connect(
    mapStateToProps, { loadBusiness })(ListBusiness);
