import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';


export class ListDepartaments extends Component {

    static propTypes = {
        departament: PropTypes.object.isRequired,
        select_departament: PropTypes.number
    }

    render() {

        if (this.props.departament.load === true){
            if (this.props.departament.list.length > 0){
                return (
                    <select className="form-control" name="departament"
                            value={this.props.select_departament} onChange={this.props.onChange}>
                        {this.props.departament.list.map((obj, index) => (
                            <option key={index} value={obj.id}>{obj.name}</option>
                            ))};
                    </select>
                );
            } else {
                return <h6 className="text-center"><strong>NO HAY DEPATAMENTOS</strong></h6>
            }

        } else {
            return <h5 className="text-center"><strong>Loading...</strong></h5>
        }

    }
}

const mapStateToProps = (state) => ({
    departament: state.departement
});

export default connect(mapStateToProps, null )(ListDepartaments);
