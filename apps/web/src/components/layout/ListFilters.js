import React from 'react';
import { connect } from 'react-redux';

import ListBusiness from "../test/ListBusiness";
import ListDepartaments from "../test/ListDepartaments";
import ListEmployee from "../test/ListEmployee";

import { loadDepartament } from '../../actions/getListDepartament';
import { loadEmployee } from '../../actions/getListEmployee';

import PropTypes from "prop-types";
import {Link} from "react-router-dom";


export class ListFilters extends React.Component {

    state = {
        business: 1,
        departament: 1,
        employee: ''
    }

    static propTypes = {
        loadDepartament: PropTypes.func.isRequired,
        loadEmployee: PropTypes.func.isRequired,
        departament: PropTypes.object.isRequired,
        employee: PropTypes.object.isRequired
    };

    componentDidMount() {
         this.props.loadDepartament(this.state.business);
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});

        if (e.target.name == 'business'){
            this.props.loadDepartament(e.target.value);
        }
    }

    searchEmployee = (e) => {
        e.preventDefault();
        this.props.loadEmployee(this.state.business, this.state.departament, this.state.employee);
    }

    render() {
        return (
            <div className="row mt-5">
                <div className="m-auto text-center col-12">
                    <h4><strong>Búsqueda de Trabajadores</strong></h4>
                    <Link
                        to={{
                            pathname: '/new',
                            state: { business: this.state.business, departament: this.state.departament}
                        }} >
                        <button className="btn btn-warning float-right">Agregar Empleado</button>
                    </Link>
                </div>
                <form className="alert-danger col-12 row py-2 mt-3" onSubmit={this.searchEmployee}>
                    <div className="col-4">
                        <h5 className="text-center">Selecionar Empresa</h5>
                        <ListBusiness
                            select_business={parseInt(this.state.business)}
                            onChange={this.onChange}
                        />
                    </div>

                    <div className="col-4">
                        <h5 className="text-center">Selecionar Departamento</h5>
                        <ListDepartaments
                            departament={this.props.departament}
                            select_departament={parseInt(this.state.departament)}
                            onChange={this.onChange}
                        />
                    </div>

                    <div className="col-3">
                        <h5 className="text-center">Filtro de Trabajadores</h5>
                        {
                            this.props.departament.load === false
                                ?
                                <h5 className="text-center"><strong>Loading...</strong></h5>
                                :
                                <input type="text" className="form-control" name="employee"
                                       value={this.state.employee} required onChange={this.onChange}/>
                        }
                    </div>
                    <div className="col-1 mt-4">
                        <button className="btn btn-danger">Buscar</button>
                    </div>
                </form>

                <section className="col-12 mt-3">
                    <ListEmployee employee={this.props.employee} />
                </section>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    departament: state.departement,
    employee: state.employee
});


export default connect(
    mapStateToProps, { loadDepartament, loadEmployee })(ListFilters);