import React, { Component, Fragment } from 'react';
import { withAlert } from 'react-alert';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

export class Alerts extends Component {
    static propTypes = {
        error: PropTypes.object.isRequired,
        message: PropTypes.object.isRequired,
    };

    componentDidUpdate(prevProps) {
        const { error, alert, message } = this.props;
        if (error !== prevProps.error) {
            if (error.msg.detail) alert.error(`${error.msg.detail.join()}`);
            if (error.msg.name) alert.error(`Nombre: ${error.msg.name.join()}`);
            if (error.msg.errors) alert.error(error.msg.errors.join());
        }

        if (message !== prevProps.message) {
            if (message.add) alert.success(message.add);
        }
    }

    render() {
        return <Fragment />;
    }
}

const mapStateToProps = (state) => ({
  error: state.errors,
  message: state.messages,
});

export default connect(mapStateToProps)(withAlert()(Alerts));