import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { add_business } from '../../../actions/add_business';


export class Dashboard extends React.Component {

    state = {
        name: ''
    }

    static propTypes = {
        add_business: PropTypes.func.isRequired
    };

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onSubmitBusiness = (e) => {
        e.preventDefault();
        this.props.add_business(this.state.name);
        this.setState({name: ''})
    }

    render() {
        return (
            <form className="alert alert-warning col-5 mx-auto" onSubmit={this.onSubmitBusiness}>
                <h5 className="text-center"><strong>Registro de Empresas</strong></h5>
                <hr></hr>
                <div className="mt-4 text-center mb-4">
                    <label>Nombre de empresa</label>
                    <input type="text" className="form-control text-center" name="name" required
                           onChange={this.onChange} value={this.state.name}/>
                </div>
                <div className="text-center mb-3">
                    <button className="btn btn-warning">Guardar</button>
                </div>
            </form>
        );
    }
}

const mapStateToProps = (state) => ({});


export default connect(mapStateToProps, { add_business })(Dashboard);