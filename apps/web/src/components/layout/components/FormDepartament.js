import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ListBusiness from '../../test/ListBusiness'
import { add_departament } from '../../../actions/addDepartament';


export class Dashboard extends React.Component {

    state = {
        name: '',
        business: 1
    }

    static propTypes = {
        add_departament: PropTypes.func.isRequired,
    };

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmitDepartament = (e) => {
        e.preventDefault();
        this.props.add_departament(this.state.business, this.state.name);
        this.setState({name: ''})
    }

    render() {
        return (
            <form className="alert alert-warning col-5 mx-auto" onSubmit={this.onSubmitDepartament}>
                <h5 className="text-center"><strong>Registro de Departamentos</strong></h5>
                <hr></hr>
                <div className="mt-4 text-center mb-4">
                    <label>Selecione Empresa</label>
                    <ListBusiness
                        select_business={parseInt(this.state.business)}
                        onChange={this.onChange}
                    />
                </div>
                <div className="mt-4 text-center mb-4">
                    <label>Nombre de empresa</label>
                    <input type="text" className="form-control text-center" name="name" required
                           onChange={this.onChange} value={this.state.name}/>
                </div>
                <div className="text-center mb-3">
                    <button className="btn btn-warning">Guardar</button>
                </div>
            </form>
        );
    }
}

const mapStateToProps = (state) => ({});


export default connect(mapStateToProps, { add_departament })(Dashboard);