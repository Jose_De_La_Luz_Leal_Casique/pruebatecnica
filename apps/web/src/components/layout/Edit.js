import React from 'react';
import { connect } from 'react-redux';
import PropTypes from "prop-types";

import { getEmployee } from '../../actions/getEmployee';
import { edit_employee } from '../../actions/updateEmployee';
import { FormatDate } from '../FormatDate';


export class Edit extends React.Component {

    state = {
        pk: 0,
        name: "",
        paternal_surname: "",
        maternal_surname: "",
        birthday: '',
        gender: 1,
        phone: '',
        cell_phone: '',
    }

    static propTypes = {
        employee: PropTypes.object.isRequired,
        edit_employee: PropTypes.func.isRequired,
    };


    componentDidMount() {
        const params = this.props.location.state;
        this.props.getEmployee(params.id);
        this.setState({ pk: params.id });
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    onSubmitUpdate = (e) =>{
        e.preventDefault();

    }


    render() {

        if (this.props.employee.loadEdit){

            return (
                <div className="row mt-5">
                    <section className="col-12">
                        <h5 className="text-center"><strong>Información Actual</strong></h5>
                        <section className="alert-danger py-3 px-2 row">
                            <div className="col-5 m-auto">
                                <h6>
                                    <strong>
                                        Nombre:
                                        {' '+this.props.employee.edit.name}
                                        {' '+this.props.employee.edit.paternal_surname}
                                        {' '+this.props.employee.edit.maternal_surname}
                                    </strong>
                                </h6>

                                <h6>
                                    <strong>
                                        Fecha de Nacimiento: {FormatDate(this.props.employee.edit.birthday)}
                                    </strong>
                                </h6>

                                <h6>
                                    <strong>
                                        Género: {this.props.employee.edit.gender}
                                    </strong>
                                </h6>
                            </div>

                             <div className="col-5 m-auto">
                                <h6>
                                    <strong>
                                        No. Telefónico:
                                        {
                                            this.props.employee.edit.phone !== null
                                                ?
                                                ' '+this.props.employee.edit.phone
                                                :
                                                ' PENDIENTE'
                                        }
                                    </strong>
                                </h6>

                                <h6>
                                    <strong>
                                        No. Celular:
                                        {
                                            this.props.employee.edit.cell_phone !== null
                                                ?
                                                ' '+this.props.employee.edit.cell_phone
                                                :
                                                ' PENDIENTE'
                                        }
                                    </strong>
                                </h6>

                                <h6>
                                    <strong>
                                        Fecha de Registro: {FormatDate(
                                            this.props.employee.edit.date_admission)}
                                    </strong>
                                </h6>
                            </div>
                        </section>
                    </section>

                    <form className="col-10 mx-auto mt-3 card py-4" onSubmit={this.onSubmitUpdate}>
                        <div className="card-body alert-primary">
                            <h6>
                                <strong>
                                    NOTA:
                                </strong>
                                Los campos vacios que sean enviados, consevan la informacion actual
                            </h6>
                        </div>

                        <section className="row">
                            <div className="col-4 mx-auto mt-3">
                                <label>Nombre: </label>
                                <input type="text" name="name" className="form-control"
                                    value={this.state.name} onChange={this.onChange}/>
                            </div>

                            <div className="col-4 mx-auto mt-3">
                                <label>Apellido Paterno:</label>
                                <input type="text" name="paternal_surname" className="form-control"
                                   value={this.state.paternal_surname} onChange={this.onChange}/>
                            </div>

                            <div className="col-4 mx-auto mt-3">
                                <label>Apellido Materno:</label>
                                <input type="text" name="maternal_surname" className="form-control"
                                   value={this.state.maternal_surname} onChange={this.onChange}/>
                            </div>

                            <div className="col-4 m-auto">
                                <label>Número Telefónico:</label>
                                <input type="text" name="phone" className="form-control"
                                   value={this.state.phone} onChange={this.onChange}/>
                            </div>

                            <div className="col-4 mx-auto mt-3">
                                <label>Número de Celular:</label>
                                <input type="text" name="name" className="form-control"
                                   value={this.state.cell_phone} onChange={this.onChange}/>
                            </div>

                        </section>
                        <div className="mt-4">
                            <button className="btn btn-primary col-2 float-right">Guardar</button>
                        </div>
                    </form>
                </div>
            );
        } else {
            return (
                <div className="mt-5 ">
                    <h2 className="text-center">Loading...</h2>
                </div>
            )
        }

    }
}

const mapStateToProps = (state) => ({
   employee: state.employee
});


export default connect(mapStateToProps, { getEmployee, edit_employee })(Edit);