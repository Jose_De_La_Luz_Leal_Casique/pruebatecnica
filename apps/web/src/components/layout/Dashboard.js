import React from 'react';

import FormBusiness from "./components/FormBusiness";
import FormDepartament from "./components/FormDepartament";
import {Link} from "react-router-dom";


export class Dashboard extends React.Component {
    render() {
        return (
            <div className="row mt-5">
                <FormBusiness />
                <FormDepartament />

                <div className="col-1 mx-auto">
                    <Link to={{ pathname: '/list'}} >
                        <button className="btn btn-warning">Listado de Empleados</button>
                    </Link>
                </div>
            </div>
        );
    }
}


export default Dashboard;