import axios from 'axios';
import { LIST_DEPARTAMENT } from './types';
import { tokenConfig } from './ConfigToken';

export const loadDepartament = (pk) => (dispatch) => {

    axios.get(`/test/departament/list/${pk}/`, tokenConfig())
        .then((res) => {
            dispatch({ type: LIST_DEPARTAMENT, payload: res.data.results });
        })
        .catch((err) => {
            switch (err.response.status){
                default:
                    console.log(err.response);
            }
        });
}
