import axios from 'axios';
import { createMessage, returnErrors } from './messages';
import { ADD_EMPLOYEE } from './types';
import { tokenConfig } from './ConfigToken';


export const add_employee = (
    name, paternal_surname, maternal_surname, birthday, gender, phone,
    cell_phone, email, password, is_administrator, business, departament) => (dispatch) => {

    const body = JSON.stringify({ name, paternal_surname, maternal_surname, birthday, gender, phone,
    cell_phone, email, password, is_administrator, business, departament });

    axios
        .post('/test/employee/add/', body, tokenConfig())
        .then((res) => {
            dispatch({type: ADD_EMPLOYEE});
            dispatch(createMessage({add: 'Departamento Registrado Correctamente'}));

        })
        .catch((err) => {
            switch (err.response.status) {
                case 400:
                    dispatch(returnErrors(err.response.data, err.response.status));
                    console.log(err.response.data)
                    break;

                default:
                    console.log(err.response.data)
                    break;
            }
        });
};