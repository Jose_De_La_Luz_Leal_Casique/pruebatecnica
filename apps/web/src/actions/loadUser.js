import axios from 'axios';
import { returnErrors } from './messages';
import { USER_LOADED, USER_LOADING, AUTH_ERROR } from './types';
import { tokenConfig } from './ConfigToken';


export const loadUser = () => (dispatch) => {
  dispatch({ type: USER_LOADING });
  axios
    .get('/accounts/get/', tokenConfig())
    .then((res) => {
      dispatch({
        type: USER_LOADED,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch(returnErrors(err.response.data, err.response.status));
      dispatch({
        type: AUTH_ERROR,
      });
    });

};