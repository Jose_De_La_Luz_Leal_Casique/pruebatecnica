import axios from 'axios';
import { GET_EMPLOYEE } from './types';
import { tokenConfig } from './ConfigToken';

export const getEmployee = (pk) => (dispatch) => {
    axios.get(`/test/employee/${pk}/`, tokenConfig())
        .then((res) => {
            dispatch({ type: GET_EMPLOYEE, payload: res.data.user });
        })
        .catch((err) => {
            switch (err.response.status){
                default:
                    console.log(err.response);
            }
        });
}
