import axios from 'axios';
import { LIST_EMPLOYEE } from './types';
import { tokenConfig } from './ConfigToken';

export const loadEmployee = (business, departament, employee) => (dispatch) => {

    axios.get(`test/employee/list/${business}/${departament}/${employee}/`, tokenConfig())
        .then((res) => {
            dispatch({ type: LIST_EMPLOYEE, payload: res.data.results });
        })
        .catch((err) => {
            switch (err.response.status){
                default:
                    console.log(err.response);
            }
        });
}
