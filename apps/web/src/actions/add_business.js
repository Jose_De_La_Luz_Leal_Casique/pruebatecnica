import axios from 'axios';
import { createMessage, returnErrors } from './messages';
import { REGISTER_BUSINESS } from './types';
import { tokenConfig } from './ConfigToken';


export const add_business = (name) => (dispatch) => {

    const body = JSON.stringify({ name });

    axios
        .post('/test/business/add/', body, tokenConfig())
        .then((res) => {
            dispatch({type: REGISTER_BUSINESS});
            dispatch(createMessage({add: 'Empresa Registrado Correctamente'}));
        })
        .catch((err) => {
            switch (err.response.status) {
                case 400:
                    dispatch(returnErrors(err.response.data, err.response.status));
                    console.log(err.response.data)
                    break;

                default:
                    console.log(err.response.data)
                    break;
            }
        });
};