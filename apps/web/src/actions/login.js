import axios from 'axios';
import { returnErrors } from './messages';
import { LOGIN_SUCCESS, LOGIN_FAIL } from './types';


export const login = (email, password) => (dispatch) => {
    const config = {
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const body = JSON.stringify({ email, password });

    axios
        .post('/accounts/login/', body, config)
        .then((res) => {
            dispatch({
                type: LOGIN_SUCCESS,
                payload: res.data,
            });
        })
        .catch((err) => {
            switch (err.response.status) {
                case 400:
                    dispatch(returnErrors(err.response.data, err.response.status));
                    dispatch({
                        type: LOGIN_FAIL,
                    });
                    break;

                case 401:
                    break;

                default:
                    console.log(err.response.data)
                    break;
            }
        });
};