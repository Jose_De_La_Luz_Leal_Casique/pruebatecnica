import axios from 'axios';
import { LIST_BUSINESS } from './types';
import { tokenConfig } from './ConfigToken';

export const loadBusiness = () => (dispatch) => {
    axios.get(`/test/business/list/`, tokenConfig())
        .then((res) => {
            dispatch({ type: LIST_BUSINESS, payload: res.data.results });
        })
        .catch((err) => {
            switch (err.response.status){
                default:
                    console.log(err.response);
            }
        });
}
