import axios from 'axios';
import { createMessage, returnErrors } from './messages';
import { UPDATE_EMPLOYEE } from './types';
import { tokenConfig } from './ConfigToken';


export const edit_employee = (pk, name, paternal_surname, maternal_surname, phone, cell_phone) => (dispatch) => {

    const body = JSON.stringify({ name, paternal_surname, maternal_surname, phone, cell_phone });

    axios
        .put(`/test/employee/edit/${pk}/`, body, tokenConfig())
        .then((res) => {
            dispatch({type: UPDATE_EMPLOYEE});
            dispatch(createMessage({add: 'Departamento Registrado Correctamente'}));

        })
        .catch((err) => {
            switch (err.response.status) {
                case 400:
                    dispatch(returnErrors(err.response.data, err.response.status));
                    console.log(err.response.data)
                    break;

                default:
                    console.log(err.response.data)
                    break;
            }
        });
};