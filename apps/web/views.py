from django.views import generic


class App(generic.TemplateView):
    template_name = 'index.html'
