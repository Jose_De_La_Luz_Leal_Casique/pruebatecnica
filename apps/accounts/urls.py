from django.urls import path

from .views import LoginView, GetUser

urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('get/', GetUser.as_view(), name='get_user'),
]