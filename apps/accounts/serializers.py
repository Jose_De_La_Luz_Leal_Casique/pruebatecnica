from rest_framework import serializers
from django.contrib.auth import authenticate

from .models import User, Profile


class ContextUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    profile = serializers.SerializerMethodField()

    @staticmethod
    def get_profile(attrs):
        return ContextUserSerializer(attrs.profile).data

    class Meta:
        model = User
        fields = ['id', 'email', 'profile']


class LoginSerializer(serializers.Serializer):
    password = serializers.CharField(write_only=True)
    email = serializers.EmailField()

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        user = authenticate(attrs, email=email, password=password)

        if user is None:
            raise serializers.ValidationError({'detail': 'Error en Credenciales'})

        return user

    @staticmethod
    def get_user(email):
        return User.objects.get(email=email)
