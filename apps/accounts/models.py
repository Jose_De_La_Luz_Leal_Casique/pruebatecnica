from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin

from apps.technical_test.models import Business, Departament
from .config import GENDER_CHOICES


class UserManager(BaseUserManager):

    def create_user(self, email, password=None):

        if email is None:
            raise TypeError('Email Requerido')

        user = self.model(email=email)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password):

        if password is None:
            raise TypeError('Se requiere Contraseña')

        user = self.create_user(email, password)
        user.is_staff = True
        user.is_superuser = True
        user.save()
        return user


class User(PermissionsMixin, AbstractBaseUser):
    business = models.ForeignKey(Business, on_delete=models.CASCADE, related_name='user', null=True)
    departament = models.ForeignKey(Departament, on_delete=models.CASCADE, related_name='user', null=True)
    email = models.EmailField(verbose_name='Correo Electrónico', null=False, blank=False, unique=True)
    is_active = models.BooleanField(default=True)
    is_administrator = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()

    def __str__(self):
        return f'{self.pk}: {self.email} -> {self.is_administrator}({self.is_active})'

    class Meta:
        ordering = ['pk']
        verbose_name = 'Usuario'
        verbose_name_plural = 'Listado de Usuarios'


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    name = models.CharField(verbose_name='Nombre (s)', null=False, blank=False, max_length=60)
    paternal_surname = models.CharField(
        verbose_name='Apellido Paterno', null=False, blank=False, max_length=60)
    maternal_surname = models.CharField(
        verbose_name='Apellido Materno', null=False, blank=False, max_length=60)
    birthday = models.DateTimeField(verbose_name='Fecha de Ingreso', null=False, blank=False)
    gender = models.CharField(
        verbose_name='Género', choices=GENDER_CHOICES, null=True, blank=True, max_length=10)
    phone = models.CharField(verbose_name='Número de Teléfono', null=True, blank=True, max_length=10)
    cell_phone = models.CharField(verbose_name='Número de Celular', null=True, blank=True, max_length=10)
    date_admission = models.DateTimeField(verbose_name='Fecha de Ingreso', auto_now_add=True)

    def __str__(self):
        return f'{self.pk}: {self.name} - {self.user.email}'

    def _get_full_name(self):
        return f'{self.name} {self.paternal_surname} {self.maternal_surname}'

    class Meta:
        ordering = ['pk']
        verbose_name = 'Perfil de Usuario'
        verbose_name_plural = 'Listado de Perfiles'
