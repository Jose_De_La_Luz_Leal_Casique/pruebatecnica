from rest_framework import generics, status
from rest_framework.response import Response
from knox.models import AuthToken
from .serializers import UserSerializer, LoginSerializer


class LoginView(generics.GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            user = serializer.get_user(serializer.data.get('email'))
            _, token = AuthToken.objects.create(user)
            return Response({
                "user": UserSerializer(user, context=self.get_serializer_context()).data,
                "token": token
            })

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GetUser(generics.RetrieveAPIView):
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user

    def retrieve(self, request, *args, **kwargs):
        user = self.get_object()
        _, token = AuthToken.objects.create(user)
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": token
        })
